#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//结果为"5"的循环
//int main()
//{
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		if (i = 5)
//			printf("%d ", i);
//	}
//	return 0;
//}

//注意case后有无break；
//int func(int a)
//{
//    int b;
//    switch (a)
//    {
//    case 1: b = 30;
//    case 2: b = 20;
//    case 3: b = 16;
//    default: b = 0;
//    }
//    return b;
//}

//int main()
//{
//    printf("%d", func(1));
//    return 0;
//}

//switch-case语句的运用
//int main() {
//	int x = 3;
//	int y = 3;
//	switch (x % 2) {
//	case 1:
//		switch (y)
//		{
//		case 0:
//			printf("first");
//		case 1:
//			printf("second");
//			break;
//		default: printf("hello");
//		}
//	case 2:
//		printf("third");
//	}
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	for (i = 3; i < 100; i++)
//	{
//		if (i % 3 == 0)
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}

//输入三个数按从大到小的顺序输出
int main()
{
	int a, b, c, temp = 0;
	scanf("%d %d %d", &a, &b, &c);
	if (a < b)
	{
		temp = a;
		a = b;
		b = temp;
	}
	if (a < c)
	{
		temp = a;
		a = c;
		c = temp;
	}
	if (b < c)
	{
		temp = b;
		b = c;
		c = temp;
	}
	printf("%d %d %d", a, b, c);
	return 0;
}

//打印100~200间的素数
//int main()
//{
//	int i = 0;
//	int j = 0;
//	for (i = 100; i < 200; i++)
//	{
//		for (j = 2; j < i; j++)
//		{
//			if (i % j == 0)
//				break;	
//		}
//		if (i == j)
//				printf("%d ", i);
//	}
//	return 0;
//}

//打印1000~2000年间的闰年；
//int main()
//{
//	int i = 0;
//	for (i = 1000; i < 2000; i++)
//	{
//		if (i % 4 == 0)
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}

//求两个数中的最大公约数
//int main()
//{
//	int a, b,temp,i;
//	scanf("%d %d", &a, &b);
//	if (a < b)
//	{
//		temp = a;
//		a = b;
//		b = temp;
//	}
//	for (i = b; i >0; i--)
//	{
//		if (a % i == 0 && b % i == 0)
//		{
//			printf("%d", i);
//			break;
//		}
//	}
//	return 0;
//}

//打印9*9乘法表
//int main()
//{
//	int i = 0;
//	int j = 0;
//	for (i = 1; i <= 9; i++)
//	{
//		for (j = 1; j <= 9; j++)
//		{
//			printf("%d*%d=%2d  ", i, j, i * j);
//		}
//		printf("\n");
//	}
//	return 0;
//}

//求10个整数的最大值；
//int main()
//{
//	int arr[10] = { 0 };
//	int MAX = 0;
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	MAX = arr[0];
//	for (i = 1; i < 10; i++)
//	{
//		if (MAX < arr[i])
//		{
//			MAX = arr[i];
//		}
//	}
//	printf("%d", MAX);
//	return 0;
//}