#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<math.h>
//int main()
//{
//	int i = 0;
//	int j = 0;
//	int a = 0;
//	scanf("%d", &a);
//	for (i = 1; i <= a; i++)
//	{
//		for (j = 1; j <= i; j++)
//		{
//			printf("%d*%d=%-2d ", i, j, i * j);
//		}
//		printf("\n");
//	}
//	return 0;
//
//}

//设计函数实现交换两个整数
//Swap(int* x,int* y )
//{
//	int temp = 0;
//	temp = *x;
//	*x = *y;
//	*y = temp;
//}
//
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//	printf("交换前：%d %d\n", a, b);
//	Swap(&a,&b);
//	printf("交换后：%d %d", a, b);
//	return 0;
//}

//设计函数判断是否为闰年

//void Run(int a)
//{
//	if ((a % 4 == 0) && (a % 100 != 0) || (a % 400 == 0))
//	{
//		printf("该年是闰年\n");
//	}
//	else
//		printf("该年不是闰年\n");
//}
//
//int main()
//{
//	int year = 0;
//	scanf("%d", &year);
//	Run(year);
//	return 0;
//}

//设计函数实现求100~200间的素数（质数）
void Prim(int x,int y)
{
	int i = 0;
	for (i = x; i <= y; i++)
	{
		int flag = 1;
		int j = 0;
		for (j = 2; j <= sqrt(i); i++)
		{
			if (i % j == 0)
				flag = 0;
				break;
		}
		if (1 == flag)
		{
			printf("%d ", i);
		}
	}
}

int main()
{
	int a = 0;
	int b = 0;
	scanf("%d %d", &a, &b);
	Prim(a, b);
	return 0;
}