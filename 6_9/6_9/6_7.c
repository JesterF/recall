//循环输入判断是否为字母
#include<stdio.h>
int main()
{
    char a;
    while ((a = getchar()) != EOF)
    {
        if (a >= 'A' && a <= 'z')
        {
            printf("YES\n");
        }
        else
        {
            printf("NO\n");
        }
    }
    return 0;
}

//打印出Hello world并输出其返回值
//#include<stdio.h>
//int main()
//{
//    int c = printf("Hello world!");
//    printf("\n%d", c);
//    return 0;
//}

//输入秒数换算出其为几时几分几秒
//#include<stdio.h>
//int main()
//{
//    int seconds, h, min, s = 0;
//    scanf("%d", &seconds);
//    h = seconds / 3600;
//    min = (seconds % 3600) / 60;
//    s = (seconds % 3600) % 60;
//    printf("%d %d %d", h, min, s);
//    return 0;
//}